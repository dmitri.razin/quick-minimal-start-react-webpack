# quick-minimal-start-react-webpack
just clone

rocket project start

```bash
git clone https://github.com/RusTorg/quick-minimal-start-react-webpack.git && cd quick-minimal-start-react-webpack && npm install
```


for develop with hot reload

```bash
npm run dev
```


for build minimized production bundle

```bash
npm run dist
```


[@rustorg](tg://resolve?domain=rustorg)
