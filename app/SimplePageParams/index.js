import React from 'react'
import { hot } from 'react-hot-loader/root'
import { Link } from "react-router-dom";

class SimplePageParams extends React.Component {




    render() {
        const link = `/post/${Math.random()}`;
        return <div>
            <h1>Page with id: {this.props.match.params.id}</h1>
            <Link to={link}>Go to another post</Link>
            {` | `}
            <Link to={`/`}>Go to home</Link>
        </div>
    }
}

export default hot(SimplePageParams)
