import React from 'react'
import { hot } from 'react-hot-loader/root'
import { Link } from "react-router-dom";

class App extends React.Component {




    render() {
        const link = `/post/${Math.random()}`;
        return <div>
            <h1>Hello!</h1>
            <Link to={link}>Go to post</Link>
        </div>
    }
}

export default hot(App)
