import React from 'react'
import ReactDOM from 'react-dom'
import {HashRouter as Router, Provider, Switch, Route} from 'react-router-dom'
import { setConfig } from 'react-hot-loader'

import 'normalize.css/normalize.css';
import './styles/main.less';
import App from "./App/index";
import SimplePageParams from "./SimplePageParams/index";




let ROUTES = {
    AppHome: (props) => (<App {...props}/>),
    SimplePage: (props) => (<SimplePageParams {...props}/>),
};


if (process.env.NODE_ENV === `development`) {


    setConfig({
        ignoreSFC: true, // RHL will be __completely__ disabled for SFC
        pureRender: true, // RHL will not change render method
    });


}

const render = () => {

    ReactDOM.render(
        <Router>
            <Switch>
                <Route exact path="/" component={ROUTES.AppHome} />
                <Route path="/post/:id" component={ROUTES.SimplePage} />

            </Switch>
        </Router>
    , document.getElementById('root'));


};

render();
