const webpack = require('webpack');

module.exports = {
    mode: 'production',
    context: __dirname + '/app',

    entry: {
        javascript: "./entryPoint.js",
    },
    output: {
        filename: 'build.js',
        path: __dirname + '/build',
    },
    optimization: {
        minimize: false,

    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
            { test: /\.less$/, use: [
                { loader: 'style-loader' },
                { loader: 'css-loader' },
                { loader: 'less-loader' },
            ] },
            { test: /\.css$/, use: [
                { loader: 'style-loader' },
                { loader: 'css-loader' },
            ] },
            {
                test: /\.(jpe?g|png|ttf|eot|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                use: 'base64-inline-loader?limit=1000&name=[name].[ext]'
            },
        ],

    },

    devServer: {
        contentBase: __dirname + '/build',
        host: '0.0.0.0',
        compress: true,
        port: 9500,
        headers: { // cors
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        }
    },

    plugins: [
        new webpack.SourceMapDevToolPlugin({
            filename: 'build.js.map',
        }),
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'development',
            DEBUG: true
        }),
    ],
};
