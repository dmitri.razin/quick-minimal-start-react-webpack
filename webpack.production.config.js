const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MinifyPlugin = require("babel-minify-webpack-plugin");


module.exports = {
    context: __dirname + '/app',
    mode: "production",
    entry: {
        javascript: "./entryPoint.js",
        //html: "./index.html"
    },
    output: {
        filename: 'build.js',
        path: __dirname + '/build'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
            { test: /\.less$/, use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                    { loader: 'less-loader' },
                ] },
            { test: /\.css$/, use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                ] },
            {
                test: /\.(jpe?g|png|ttf|eot|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                use: 'base64-inline-loader?limit=1000&name=[name].[ext]'
            }
        ],

    },
    optimization: {
        minimize: true,
        minimizer: [new UglifyJsPlugin({
            include: /\.min\.js$/
        })]

    },
    plugins: [
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'production', // use 'development' unless process.env.NODE_ENV is defined
            DEBUG: false
        }),
        new MinifyPlugin()
    ]

};
